# Simple password generator  
This program generates 216 random words.  
You can generate a new set of words with spacebar.  
To generate a password, roll 3 dice and pick the corresponding word on the screen, then press spacebar and repeat for as many words as you like.  
This method means the password generated is partly physical and partly digital, there is no interaction that could give away the word needed (such as scrolling or searching for the number).  
If you want to be extra secure, you can generate 6 words and use another die to pick one.  
