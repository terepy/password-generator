use ::rand::thread_rng;
use ::rand::seq::SliceRandom;
use macroquad::prelude::*;
use std::error::Error;

const MIN_FREQ: u64 = 100_000;
const MAX_LEN: usize = 8;

#[macroquad::main("hexgame")]
async fn main() -> Result<(), Box<dyn Error>> {
    let raw = include_str!("wordlist");
    let wordlist: Vec<_> = raw.lines()
        .filter_map(|line| {
            let mut iter = line.split_whitespace();
            let word = iter.next()?;
            let freq: u64 = iter.next()?.parse().ok()?;
            Some((word, freq))
        })
        .take_while(|&(_, freq)| freq >= MIN_FREQ)
        .filter(|&(word, _)| word.len() <= MAX_LEN)
        .collect();

    let mut rng = thread_rng();
    let mut words = wordlist.choose_multiple(&mut rng, 216).map(|x| x.0).collect::<Vec<_>>();
    
    loop {
        display_words(&words);

        if is_key_pressed(KeyCode::Enter) {
            words = wordlist.choose_multiple(&mut rng, 216).map(|x| x.0).collect::<Vec<_>>();
        }

        if is_key_pressed(KeyCode::Escape) {
            break Ok(());
        }
        
        next_frame().await;
    }
}

fn display_words(words: &[&str]) {
    clear_background(BLACK);
    let table_size = 36;
    let tables = 6;
    let mut ypos = 20.0;

    for table in 0..tables {
        let mut xpos = 10.0;
        draw_text(&format!("{}", table + 1), xpos, ypos, 20.0, WHITE);
        ypos += 20.0;

        for i in 0..table_size {
            let word = words[i + table_size * table];
            let x = i % 6 + 1;
            let y = i / 6 + 1;
            draw_text(&format!("{x}{y} {word}"), xpos, ypos, 20.0, WHITE);
            xpos += 130.0;
            if x == 6 {
                xpos = 10.0;
                ypos += 20.0;
            }
        }
        ypos += 20.0;
    }
}
